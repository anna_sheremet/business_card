import React from "react";

export default function Footer() {
    return (
        <div className="footer">
            <div className="footer-links">
                <a href="https://t.me/ann_sheremet">
                    <i className="fa-solid fa-paper-plane"></i>
                </a>
                <a href="https://www.facebook.com/profile.php?id=100041004465540">
                    <i className="fa-brands fa-facebook"></i>
                </a>
                <a href="https://instagram.com/an.na19sher?igshid=ZDdkNTZiNTM=">
                    <i className="fa-brands fa-square-instagram"></i>
                </a>
                <a href="https://gitlab.com/anna_sheremet">
                    <i className="fa-brands fa-gitlab"></i>
                </a>
            </div>
        </div>
    )
}