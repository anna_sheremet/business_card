import React from "react";
import TopContent from "./TopCotent.jsx";
import Information from "./Information.jsx";
import Footer from "./Footer.jsx";
function App() {

  return (
    <div className="content">
      <TopContent />
      <Information />
      <Footer />
    </div>
  )
}

export default App
