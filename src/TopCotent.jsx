import React from "react";
import photo from "./assets/personal_photo.png"


export default function TopContent() {
    return (
        <div className="header">
            <div className="header-image-keeper">
                <img src={photo} className="header-image" alt="photo" />
            </div>
            <div className="header-info">
                <div className="header-info-about">
                    <h1>Anna Sheremet</h1>
                    <p id="description">Python Developer</p>
                </div>
                <div className="header-info-links">
                    <div className="header-buttons-keeper">
                        <a className="header-button" href="mailto:an.scheremet2013@gmail.com?subject='Candidate revision'">
                            <button>
                                <i className="fa-solid fa-envelope header-icon"></i>
                                Gmail
                            </button>
                        </a>
                        <a className="header-button" href="www.linkedin.com/in/anna-sheremet-586720240">
                            <button>
                                <i className="fa-brands fa-linkedin header-icon"></i>
                                LinkedIn
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}