import React from "react";

export default function Information() {
    return (
        <div className="info">
            <div className="info-section">
                <h3 className="info-title">About</h3>
                <span className="info-text">
                    Hard-working, creative and proactive. Study Computer science "Cyber security". Create web sites on Python/Django. Constantly developing new skills and abilities
                </span>
            </div>
            <div className="info-section">
                <h3 className="info-title">Interests</h3>
                <span className="info-text">
                    Food expert. Art scholar. Reader. Internet guru. Travel geek. Coffee fanatic. Climber.
                </span>
            </div>
        </div>
    )
}